<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SmartWheels</title>
	<meta name="description" content="SmartWheels">
	<meta name="keywords" content="SmartWheels">
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>
		
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="viewport" content="width=device-width, user-scalable=no">

	<link rel="stylesheet" href="css/style.css">	
	<link rel="stylesheet" href="css/responsive.css">	
	
	<script src="js/jquery-1.11.1.min.js"></script>

</head>
<body class="confirmpage">
<div class="confirmtext">Данной страницы не существует<br><br><a href="/">Перейти на сайт</a></div>

<?php include('counters.php'); ?>

<script>
$( document ).ready(function() {
    setTimeout(function() {
		window.location.href = "/";
	}, 5000);
});
</script>
</body>
</html>