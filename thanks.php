﻿<?php /*
--------------------------
Обработчик всех форм сайта
--------------------------

Для коректной работы формы
заполните все настройки.
--------------------------
*/

/*НАСТРОЙКИ ПОЧТЫ*/
$AdminEmail='admin@smart-wheels.club'; //Почта куда будут приходить все письма с сайта
$from='noreply@smart-wheels.club'; //От кого письмо, обычно это название компании или имя человека.
/*КОНЕЦ НАСТРОЕК ПОЧТЫ*/

$headers  = "Content-type: text/html; charset=UTF-8 \r\n";
$headers .= "From: $from\r\n"; 

$message = "<p>ip: {$_COOKIE["ip"]}</p><br><br>";
$email_to ="bani2.-.smart-wheels.club@lptracker.ru";

/*Форма перезвоните мне*/
 if($_POST['enter_callme']){
	$subject = 'Новая заявка перезвонить! - smart-wheels'; //тема письма
	$text =  'Перезвоните мне!<br><br> Телефон: '.$_POST['phone'].'<br>'; //текст письма
    mail($AdminEmail, $subject, $text, $headers);
	$message .= $text;
	mail($email_to, $subject, $message, $headers);
	$goodtext = 'Спасибо, ваша заявка принята. Наши менеджеры свяжутся с вами в ближайшее время'; //Текст уведомления о успешной операции
}

/*Форма заказа*/
if($_POST['enter_request_sigway']){
	if($_POST['complectation'] == 1){ $complectation='Красный'; } //выбор комплектации - первое изображение
	if($_POST['complectation'] == 2){ $complectation='Синий'; } //выбор комплектации - второе изображение
	if($_POST['complectation'] == 3){ $complectation='Черный'; } //выбор комплектации - первое изображение
	$subject = 'Новый заказ! - smart-wheels'; //тема письма
	//текст письма
	$text =  "Форма заказа:<br><br>";
	if($_POST['name']) $text .=  "Имя: $_POST[name]<br>";
	if($_POST['phone']) $text .=  "Телефон: $_POST[phone]<br>";
	if($_POST['customerphone'] && $_POST['phone'] != $_POST['customerphone']) $text .=  "Телефон: $_POST[customerphone]<br>"; 
	if($complectation) $text .=  "Тип комплектации: $complectation<br>"; 
    mail($AdminEmail, $subject, $text, $headers);
	$message .= $text;
	mail($email_to, $subject, $message, $headers);
	$goodtext = 'Спасибо, ваша заявка принята. Наши менеджеры свяжутся с вами в ближайшее время'; //Текст уведомления о успешной операции
}

/*Форма отзыва*/
if($_POST['enter_comment_window']){
	$subject = 'Новый отзыв - smart-wheels'; //тема письма
	//текст письма
	$text =  "Отзыв:<br><br>
	Имя: $_POST[name]<br>
	Отзыв: $_POST[comment]<br>"; 
    mail($AdminEmail, $subject, $text, $headers);
	$message .= $text;
	mail($email_to, $subject, $message, $headers);
	$goodtext = 'Спасибо, Ваш отзыв принят. Мы опубликуем его после проверки модератором'; //Текст уведомления о успешной операции
}
if($_POST['enter_friend_window']){
	$subject = 'Ваш друг оставил Вам сообщение - SmartWheels'; //тема письма
	//текст письма
	$text =  "Здравствуйте!<br><br>
	Ваш друг оставил Вашу почту, чтобы Вы посмотрели на новинку на рынке!<br>
	Этот невероятное средство передвижения уже на наших складах в России!<br>
	Посмотрите на нашем сайте: http://smart-wheels.club/ <br>"; 
    mail($_POST['email'], $subject, $text, $headers);
	$message .= $text;
	mail($email_to, $subject, $message, $headers);
	$goodtext = 'Спасибо, вашему другу отправлено сообщение на указанную электронную почту.'; //Текст уведомления о успешной операции
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SmartWheels</title>
	<meta name="description" content="SmartWheels">
	<meta name="keywords" content="SmartWheels">
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>
		
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="viewport" content="width=device-width, user-scalable=no">

	<link rel="stylesheet" href="css/style.css">	
	<link rel="stylesheet" href="css/responsive.css">	
	
	<script src="js/jquery-1.11.1.min.js"></script>

</head>
<body class="confirmpage">
<div class="confirmtext"><?php echo $goodtext; ?></div>

<?php include('counters.php'); ?>

<script>
$( document ).ready(function() {
    setTimeout(function() {
		window.location.href = "/";
	}, 5000);
});
</script>
</body>
</html>