$(function(){
	
	var note = $('#note'),
		ts = new Date(2012, 0, 1),
		newYear = true;
	
	if((new Date()) > ts){
		// The new year is here! Count towards something else.
		// Notice the *1000 at the end - time must be in milliseconds
		ts = (new Date()).getTime() + 124456*1000;
		newYear = false;
	}
		
	$('#countdown').countdown({
		timestamp	: ts,
		callback	: function(days, hours, minutes, seconds){
			
			var message = "";
			
			message += days + " дней" + ( days==1 ? '':'' ) + ", ";
			message += hours + " час" + ( hours==1 ? '':'ов' ) + ", ";
			message += minutes + " минут" + ( minutes==1 ? 'а':'' ) + ", ";
			message += seconds + " секунд" + ( seconds==1 ? 'а':'' ) + " <br />";
			
			
			note.html(message);
		}
	});	
});
