function fullscreen(){	
	if(document.getElementById('butt_close').style.display=="block"){
		document.getElementById('butt_close').style.display="none";
		document.getElementById('have_to_try').style.display="block";
		document.getElementById('button_lb').style.display="block";
		document.getElementById('ax2').style.display="block";
		document.getElementById('watch').style.display="block";
		document.getElementById('drawer_button').style.display="block";
		document.getElementById('hm_logo').style.display="block";
		document.getElementById('body').style.overflow="visible";
		document.getElementById('ax3').style.display="block";
		document.getElementById('general_phone_video').style.display="block";
	}else{
		document.getElementById('butt_close').style.display="block";
		document.getElementById('have_to_try').style.display="none";
		document.getElementById('button_lb').style.display="none";
		document.getElementById('ax2').style.display="none";
		document.getElementById('watch').style.display="none";
		document.getElementById('drawer_button').style.display="none";
		document.getElementById('hm_logo').style.display="none";
		document.getElementById('body').style.overflow="hidden";
		document.getElementById('ax3').style.display="none";
		document.getElementById('general_phone_video').style.display="none";
	}
}
function edit_color(_id){
	if(_id==1){
		source='/images/equip_red.png'; 
		small_source1='/images/equip_red_min.png';
		small_source2='/images/equip_red_2_min.png'; 
		small_source3='/images/equip_red_3_min.png'; 
		small_source4='/images/equip_red_4_min.png'; }
	if(_id==2){
		source='/images/equip_blue.png'; 
		small_source1='/images/equip_blue_min.png';
		small_source2='/images/equip_blue_2_min.png'; 
		small_source3='/images/equip_blue_3_min.png'; 
		small_source4='/images/equip_blue_4_min.png';}
	if(_id==3){
		source='/images/equip_black.png'; 
		small_source1='/images/equip_black_min.png';
		small_source2='/images/equip_black_2_min.png'; 
		small_source3='/images/equip_black_3_min.png'; 
		small_source4='/images/equip_black_4_min.png';}
		
	document.getElementById('main_source').src=source;
	document.getElementById('small_source1').src=small_source1;
	document.getElementById('small_source2').src=small_source2;
	document.getElementById('small_source3').src=small_source3;
	document.getElementById('small_source4').src=small_source4;
}
function edit_source(_id){
	var source=document.getElementById('small_source'+_id).src;
	var _length = (source.length-8);
	var url=source.substr(0,_length);
	var type=source.substr(-4);
	source=url+type;
	document.getElementById('main_source').src=source;
}
function smallvideo(_id){
	var w = screen.width;
	if(w<=440){
	if(_id==1){source='<iframe src="https://www.youtube.com/embed/odksM1xDSJM?rel=0&amp;showinfo=0&autoplay=1" frameborder="0" allowfullscreen></iframe>';}
	if(_id==2){source='<iframe src="https://www.youtube.com/embed/uLJNcNBS1ig?rel=0&amp;showinfo=0&autoplay=1" frameborder="0" allowfullscreen></iframe>';}
	if(_id==3){source='<iframe src="https://www.youtube.com/embed/aWklK427o9E?rel=0&amp;showinfo=0&autoplay=1" frameborder="0" allowfullscreen></iframe>';}
	if(_id==4){source='<iframe src="https://www.youtube.com/embed/3P9R4CZIFfQ?rel=0&amp;showinfo=0&autoplay=1" frameborder="0" allowfullscreen></iframe>';}
	if(_id==5){source='<iframe src="https://www.youtube.com/embed/RjeoNV1dE28?rel=0&amp;showinfo=0&autoplay=1" frameborder="0" allowfullscreen></iframe>';}
	if(_id==6){source='<iframe src="https://www.youtube.com/embed/EplwWCxuIBw?rel=0&amp;showinfo=0&autoplay=1" frameborder="0" allowfullscreen></iframe>';}
	if(_id==7){source='<iframe src="https://www.youtube.com/embed/OixHlSec4PM?rel=0&amp;showinfo=0&autoplay=1" frameborder="0" allowfullscreen></iframe>';}
	if(_id==8){source='<iframe src="https://www.youtube.com/embed/I56Xufecp1g?rel=0&amp;showinfo=0&autoplay=1" frameborder="0" allowfullscreen></iframe>';}
	document.getElementById('window_video').innerHTML=source;
	document.getElementById('window_video').style.display = 'block';
	document.getElementById('close').style.display = 'block';
	}else{
	if(_id==1){source='<iframe src="https://www.youtube.com/embed/odksM1xDSJM?rel=0&amp;showinfo=0&autoplay=1" frameborder="0" allowfullscreen></iframe>';}
	if(_id==2){source='<iframe src="https://www.youtube.com/embed/uLJNcNBS1ig?rel=0&amp;showinfo=0&autoplay=1" frameborder="0" allowfullscreen></iframe>';}
	if(_id==3){source='<iframe src="https://www.youtube.com/embed/aWklK427o9E?rel=0&amp;showinfo=0&autoplay=1" frameborder="0" allowfullscreen></iframe>';}
	if(_id==4){source='<iframe src="https://www.youtube.com/embed/3P9R4CZIFfQ?rel=0&amp;showinfo=0&autoplay=1" frameborder="0" allowfullscreen></iframe>';}
	if(_id==5){source='<iframe src="https://www.youtube.com/embed/RjeoNV1dE28?rel=0&amp;showinfo=0&autoplay=1" frameborder="0" allowfullscreen></iframe>';}
	if(_id==6){source='<iframe src="https://www.youtube.com/embed/EplwWCxuIBw?rel=0&amp;showinfo=0&autoplay=1" frameborder="0" allowfullscreen></iframe>';}
	if(_id==7){source='<iframe src="https://www.youtube.com/embed/OixHlSec4PM?rel=0&amp;showinfo=0&autoplay=1" frameborder="0" allowfullscreen></iframe>';}
	if(_id==8){source='<iframe src="https://www.youtube.com/embed/I56Xufecp1g?rel=0&amp;showinfo=0&autoplay=1" frameborder="0" allowfullscreen></iframe>';}
	document.getElementById('smallvideo_'+_id).innerHTML=source;
	}
}